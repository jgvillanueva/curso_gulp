# Ejercicio práctico
Agenda de contactos básica con JavaScript, usando jQuery como librería de acceso al DOM y utilidades y storejs para el almacenamiento persistente.

## Tareas Gulp disponibles

### Para empezar a trabajar en el proyecto
`gulp`  
Borrado inicial de dist, build inicial de la aplicación y serve de la aplicación. Añadido de watchers para procesar los cambios realizados.

### Otras tareas disponibles
`gulp clean`  
Borrado de carpeta de distribución

`gulp html`  
Copia de ficheros .html raíz de src a dist

`gulp vendor`  
Copia de librerías vendor de src a dist

`gulp data`  
Copia del fichero de configuración inicial de la aplicación

`gulp imagemin`  
Copia y optimización de imágenes de src a dist 

`gulp scss`  
Transpilación de scss a dist

`gulp lint`  
Depuración de código JS

`gulp js`  
Concatenación, Minificación y Ofuscación de JS

`gulp serve`  
Build inicial, serve y añadido de wathcers para procesar cambios.

`gulp serveXAMPP`  
Build inicial, serve utilizando XAMPP y añadido de wathcers para procesar cambios.

